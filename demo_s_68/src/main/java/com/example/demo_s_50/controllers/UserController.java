package com.example.demo_s_50.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s_50.model.CUser;
import com.example.demo_s_50.repository.IOrderRepository;
import com.example.demo_s_50.repository.IUserRepository;

@CrossOrigin
@RestController
public class UserController {
    @Autowired
    IUserRepository pUserRepository;

    @Autowired
    IOrderRepository pOrderRepository;

    @PostMapping("/user/create")
    public ResponseEntity<Object> createUser(@RequestBody CUser cUser) {
        try {

            CUser newRole = new CUser();
            newRole.setFullname(cUser.getFullname());
            newRole.setEmail(cUser.getEmail());
            newRole.setPhone(cUser.getPhone());
            newRole.setAddress(cUser.getAddress());
            newRole.setCreated(new Date());
            newRole.setUpdated(null);
            CUser savedRole = pUserRepository.save(newRole);

            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/user/update/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") Long id, @RequestBody CUser cUser) {
        Optional<CUser> userData = pUserRepository.findById(id);
        if (userData.isPresent()) {

            CUser newUser = userData.get();
            newUser.setFullname(cUser.getFullname());
            newUser.setEmail(cUser.getEmail());
            newUser.setPhone(cUser.getPhone());
            newUser.setAddress(cUser.getAddress());
            newUser.setUpdated(new Date());
            CUser savedCountry = pUserRepository.save(newUser);

            return new ResponseEntity<>(savedCountry, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/user/delete/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable Long id) {
        try {
            Optional<CUser> optional = pUserRepository.findById(id);
            if (optional.isPresent()) {
                pUserRepository.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/user/details/{id}")
    public CUser getUserById(@PathVariable Long id) {
        if (pUserRepository.findById(id).isPresent())
            return pUserRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/user/all")
    public List<CUser> getAllUsers() {
        return pUserRepository.findAll();
    }
}