package com.example.demo_s_50.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s_50.model.CCountry;
import com.example.demo_s_50.model.CDistrict;
import com.example.demo_s_50.model.COrder;
import com.example.demo_s_50.model.CRegion;
import com.example.demo_s_50.repository.CountryRepository;
import com.example.demo_s_50.repository.IDistrictRepository;
import com.example.demo_s_50.repository.RegionRepository;

@CrossOrigin
@RestController
public class DistrictController {
    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private IDistrictRepository pDistrictRepository;

    @PostMapping("/district/create/{id}")
    public ResponseEntity<Object> createDistrict(@PathVariable("id") Long id, @RequestBody CDistrict cDistrict) {
        try {
            Optional<CRegion> regionData = regionRepository.findById(id);
            if (regionData.isPresent()) {

                CDistrict newRole = new CDistrict();
                newRole.setDistrictCode(cDistrict.getDistrictCode());
                newRole.setDistrictName(cDistrict.getDistrictName());
                newRole.setRegion(cDistrict.getRegion());

                CRegion _district = regionData.get();
                newRole.setRegion(_district);

                CDistrict savedRole = pDistrictRepository.save(newRole);

                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/district/update/{id}")
    public ResponseEntity<Object> updateDistrict(@PathVariable("id") Long id, @RequestBody CDistrict cDistrict) {
        Optional<CDistrict> districtData = pDistrictRepository.findById(id);
        if (districtData.isPresent()) {

            CDistrict newDistrict = districtData.get();
            newDistrict.setDistrictCode(cDistrict.getDistrictCode());
            newDistrict.setDistrictName(cDistrict.getDistrictName());
            CDistrict savedDistrict = pDistrictRepository.save(newDistrict);

            return new ResponseEntity<>(savedDistrict, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/district/delete/{id}")
    public ResponseEntity<Object> deleteDistrictById(@PathVariable Long id) {
        try {
            pDistrictRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/district/details/{id}")
    public CDistrict getDistrictsById(@PathVariable Long id) {
        if (pDistrictRepository.findById(id).isPresent())
            return pDistrictRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/district/all")
    public List<CDistrict> getAllDistrict() {
        return pDistrictRepository.findAll();
    }

    @GetMapping("/region/{regionId}/districts")
    public List<CDistrict> getDistrictsByRegion(@PathVariable(value = "regionId") Long regionId) {
        return pDistrictRepository.findByRegionId(regionId);
    }
}
