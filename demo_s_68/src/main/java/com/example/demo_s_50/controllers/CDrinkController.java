package com.example.demo_s_50.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s_50.model.CDrink;
import com.example.demo_s_50.repository.IDrinkRepository;

@CrossOrigin
@RestController
public class CDrinkController {
	@Autowired
	IDrinkRepository pDrinkRepository;

	// GET tất cả dữ liệu
	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getAllDrinks() {
		try {
			List<CDrink> pVouchers = new ArrayList<CDrink>();
			// TODO: Hãy code lấy danh sách drinks từ DB
			pDrinkRepository.findAll().forEach(pVouchers::add);
			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// GET dữ liệu theo id
	@GetMapping("/drinks/{id}")
	public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id) {
		try {
			Optional<CDrink> drinkData = pDrinkRepository.findById(id);
			// Todo: viết code lấy drink theo id tại đây
			if (drinkData.isPresent()) {
				return new ResponseEntity<>(drinkData.get(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Tạo MỚI drink KHÔNG dùng service sử dụng phương thức POST
	@PostMapping("/drinks")
	public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pCDrinks) {
		try {
			// TODO: Hãy viết code tạo drink đưa lên DB
			Optional<CDrink> drinkData = pDrinkRepository.findById(pCDrinks.getId());
			if (drinkData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Drink already exsit  ");
			}
			CDrink cDrink = new CDrink();
			cDrink.setTenNuocUong(pCDrinks.getTenNuocUong());
			cDrink.setMaNuocUong(pCDrinks.getMaNuocUong());
			cDrink.setDonGia(pCDrinks.getDonGia());
			cDrink.setGhiChu(pCDrinks.getGhiChu());
			cDrink.setNgayTao(new Date());
			cDrink.setNgayCapNhat(null);
			CDrink _drinks = pDrinkRepository.save(cDrink);
			return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			// return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
		}
	}

	// Sửa/update drink theo {id} KHÔNG dùng service, sử dụng phương thức PUT
	@PutMapping("/drinks/{id}") // Dùng phương thức PUT
	public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @RequestBody CDrink pCDrinks) {
		try {
			// TODO: Hãy viết code lấy drink từ DB để UPDATE
			Optional<CDrink> drinkData = pDrinkRepository.findById(id);
			if (drinkData.isPresent()) {
				CDrink drink = drinkData.get();
				drink.setMaNuocUong(pCDrinks.getMaNuocUong());
				drink.setTenNuocUong(pCDrinks.getTenNuocUong());
				drink.setDonGia(pCDrinks.getDonGia());
				drink.setGhiChu(pCDrinks.getGhiChu());
				drink.setNgayCapNhat(new Date());

				return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
			} else {
				return ResponseEntity.badRequest().body("Failed to get specified Drink: " + id + "  for update.");
			}
		} catch (Exception e) {
			System.out.println(e);
			// return ResponseEntity.unprocessableEntity().body("Failed to Update specified
			// Voucher:"+e.getCause().getCause().getMessage());
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Xoá/delete drink theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/drinks/{id}") // Dùng phương thức DELETE
	public ResponseEntity<CDrink> deleteDrinkById(@PathVariable("id") long id) {
		try {
			pDrinkRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
