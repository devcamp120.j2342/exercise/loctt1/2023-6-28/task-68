package com.example.demo_s_50.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.example.demo_s_50.model.CVoucher;
import com.example.demo_s_50.repository.IVoucherRepository;
import com.example.demo_s_50.service.CVoucherService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CVoucherController {
	@Autowired
	IVoucherRepository pVoucherRepository;
	@Autowired
	CVoucherService pVoucherService;

	// Lấy danh sách voucher dùng service.
	@GetMapping("/vouchers") // Dùng phương thức GET
	public ResponseEntity<List<CVoucher>> getAllVouchersBySevice() {
		try {
			return new ResponseEntity<>(pVoucherService.getVoucherList(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// Lấy danh sách voucher KHÔNG dùng service
	@GetMapping("/all-vouchers") // Dùng phương thức GET
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			List<CVoucher> vouchers = new ArrayList<CVoucher>();
			pVoucherRepository.findAll().forEach(vouchers::add);
			return new ResponseEntity<>(vouchers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// Lấy voucher theo {id} KHÔNG dùng service
	@GetMapping("/vouchers/{id}") // Dùng phương thức GET
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// Tạo MỚI voucher dùng service sử dụng phương thức POST
	@PostMapping("/post-vouchers") // Dùng phương thức POST
	public ResponseEntity<Object> createServiceCVoucher(@RequestBody CVoucher pVouchers) {
		try {
			Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
			if (voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}
			return new ResponseEntity<>(pVoucherService.postVoucherList(pVouchers), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	// Tạo MỚI voucher KHÔNG dùng service sử dụng phương thức POST
	@PostMapping("/vouchers") // Dùng phương thức POST
	public ResponseEntity<Object> createCVoucher(@RequestBody CVoucher pVouchers) {
		try {

			Optional<CVoucher> voucherData = pVoucherRepository.findById(pVouchers.getId());
			if (voucherData.isPresent()) {
				return ResponseEntity.unprocessableEntity().body(" Voucher already exsit  ");
			}
			pVouchers.setNgayTao(new Date());
			pVouchers.setNgayCapNhat(null);
			CVoucher _vouchers = pVoucherRepository.save(pVouchers);
			return new ResponseEntity<>(_vouchers, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			// Hiện thông báo lỗi tra back-end
			// return new ResponseEntity<>(e.getCause().getCause().getMessage(),
			// HttpStatus.INTERNAL_SERVER_ERROR);
			return ResponseEntity.unprocessableEntity()
					.body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
		}
	}

	// Sửa/update voucher theo {id} dùng service, sử dụng phương thức PUT
	@PutMapping("/put-vouchers/{id}") // Dùng phương thức PUT
	public ResponseEntity<Object> updateServiceCVoucherById(@PathVariable("id") long id,
			@RequestBody CVoucher pVouchers) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			try {
				return new ResponseEntity<>(pVoucherRepository.save(pVoucherService.putVoucherList(id, pVouchers)),
						HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
			}
		} else {
			return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
		}
	}

	// Sửa/update voucher theo {id} KHÔNG dùng service, sử dụng phương thức PUT
	@PutMapping("/vouchers/{id}") // Dùng phương thức PUT
	public ResponseEntity<Object> updateCVoucherById(@PathVariable("id") long id, @RequestBody CVoucher pVouchers) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			CVoucher voucher = voucherData.get();
			voucher.setMaVoucher(pVouchers.getMaVoucher());
			voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			voucher.setNgayCapNhat(new Date());
			try {
				return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
			}
		} else {
			return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
		}
	}

	// Xoá/delete voucher theo {id} dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/delete-vouchers/{id}") // Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteServiceCVoucherById(@PathVariable("id") long id) {
		try {
			return pVoucherService.deleteVoucherList(id);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Xoá/delete voucher theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/vouchers/{id}") // Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
	@DeleteMapping("/vouchers") // Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteAllCVoucher() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
