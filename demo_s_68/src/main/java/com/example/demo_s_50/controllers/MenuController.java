package com.example.demo_s_50.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s_50.model.CDrink;
import com.example.demo_s_50.model.CMenu;
import com.example.demo_s_50.repository.IMenuRepository;

@CrossOrigin
@RestController
public class MenuController {
    @Autowired
    IMenuRepository pMenuRepository;

    @GetMapping("/menus")
    public ResponseEntity<List<CMenu>> getAllMenus() {
        try {
            List<CMenu> pCMenus = new ArrayList<CMenu>();

            pMenuRepository.findAll().forEach(pCMenus::add);

            return new ResponseEntity<>(pCMenus, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // GET dữ liệu theo id
    @GetMapping("/menus/{id}")
    public ResponseEntity<CMenu> getMenuById(@PathVariable("id") long id) {
        try {
            Optional<CMenu> menuData = pMenuRepository.findById(id);
            // Todo: viết code lấy drink theo id tại đây
            if (menuData.isPresent()) {
                return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Tạo MỚI menus KHÔNG dùng service sử dụng phương thức POST

    @PostMapping("/menus")
    public ResponseEntity<Object> createMenu(@RequestBody CMenu pCMenu) {
        try {
            // TODO: Hãy viết code tạo drink đưa lên DB
            Optional<CMenu> menuData = pMenuRepository.findById(pCMenu.getId());
            if (menuData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body(" Menu already exsit  ");
            }
            CMenu cMenu = new CMenu();
            cMenu.setSize(pCMenu.getSize());
            cMenu.setDuongKinh(pCMenu.getDuongKinh());
            cMenu.setSuon(pCMenu.getSuon());
            cMenu.setSalad(pCMenu.getSalad());
            cMenu.setSoLuongNuocNgot(pCMenu.getSoLuongNuocNgot());
            cMenu.setDonGia(pCMenu.getDonGia());
            CMenu _menus = pMenuRepository.save(cMenu);
            return new ResponseEntity<>(_menus, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    // Sửa/update menus theo {id} KHÔNG dùng service, sử dụng phương thức PUT
    @PutMapping("/menus/{id}") // Dùng phương thức PUT
    public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @RequestBody CMenu pCMenu) {
        try {
            // TODO: Hãy viết code lấy drink từ DB để UPDATE
            Optional<CMenu> menuData = pMenuRepository.findById(id);
            if (menuData.isPresent()) {
                CMenu cMenu = menuData.get();
                cMenu.setSize(pCMenu.getSize());
                cMenu.setDuongKinh(pCMenu.getDuongKinh());
                cMenu.setSuon(pCMenu.getSuon());
                cMenu.setSalad(pCMenu.getSalad());
                cMenu.setSoLuongNuocNgot(pCMenu.getSoLuongNuocNgot());
                cMenu.setDonGia(pCMenu.getDonGia());
                return new ResponseEntity<>(pMenuRepository.save(cMenu), HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest().body("Failed to get specified Menu: " + id + "  for update.");
            }
        } catch (Exception e) {
            System.out.println(e);
            // return ResponseEntity.unprocessableEntity().body("Failed to Update specified
            // Voucher:"+e.getCause().getCause().getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Xoá/delete drink theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/menus/{id}") // Dùng phương thức DELETE
    public ResponseEntity<CDrink> deleteDrinkById(@PathVariable("id") long id) {
        try {
            pMenuRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
