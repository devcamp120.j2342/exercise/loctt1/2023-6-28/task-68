package com.example.demo_s_50.controllers;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_s_50.model.COrder;
import com.example.demo_s_50.model.CUser;
import com.example.demo_s_50.repository.IOrderRepository;
import com.example.demo_s_50.repository.IUserRepository;

@CrossOrigin
@RestController
public class OrderController {
    @Autowired
    IUserRepository pUserRepository;

    @Autowired
    IOrderRepository pOrderRepository;

    @PostMapping("/order/create/{id}")
    public ResponseEntity<Object> createOrder(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
        try {
            Optional<CUser> userData = pUserRepository.findById(id);
            if (userData.isPresent()) {

                COrder newRole = new COrder();
                newRole.setOrderCode(cOrder.getOrderCode());
                newRole.setPizzaType(cOrder.getPizzaType());
                newRole.setPizzaSize(cOrder.getPizzaSize());
                newRole.setPaid(cOrder.getPaid());
                newRole.setPrice(cOrder.getPrice());
                newRole.setVoucherCode(cOrder.getVoucherCode());
                newRole.setCreated(new Date());
                newRole.setUpdated(null);
                newRole.setCUser(cOrder.getCUser());

                CUser _user = userData.get();
                newRole.setCUser(_user);

                COrder savedRole = pOrderRepository.save(newRole);

                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/order/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable("id") Long id, @RequestBody COrder cOrder) {
        Optional<COrder> orderData = pOrderRepository.findById(id);
        if (orderData.isPresent()) {

            COrder newOrder = orderData.get();
            newOrder.setOrderCode(cOrder.getOrderCode());
            newOrder.setPizzaType(cOrder.getPizzaType());
            newOrder.setPizzaSize(cOrder.getPizzaSize());
            newOrder.setPaid(cOrder.getPaid());
            newOrder.setPrice(cOrder.getPrice());
            newOrder.setVoucherCode(cOrder.getVoucherCode());
            newOrder.setUpdated(new Date());
            COrder savedOrder = pOrderRepository.save(newOrder);

            return new ResponseEntity<>(savedOrder, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable Long id) {
        try {
            pOrderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order/details/{id}")
    public COrder getOrdersById(@PathVariable Long id) {
        if (pOrderRepository.findById(id).isPresent())
            return pOrderRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/order/all")
    public List<COrder> getAllOrder() {
        return pOrderRepository.findAll();
    }

    @GetMapping("/user/{userId}/orders")
    public List<COrder> getOrdersByUser(@PathVariable(value = "userId") Long userId) {
        return pOrderRepository.findByCUserId(userId);
    }

    @GetMapping("/user/{userId}/orders/{id}")
    public Optional<COrder> getOrderByOrderAndUser(@PathVariable(value = "userId") Long userId,
            @PathVariable(value = "id") Long orderId) {
        return pOrderRepository.findByIdAndCUserId(orderId, userId);
    }
}
