package com.example.demo_s_50;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoS50Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoS50Application.class, args);
	}

}
