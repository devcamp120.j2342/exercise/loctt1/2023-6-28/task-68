package com.example.demo_s_50.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo_s_50.model.CCountry;
import com.example.demo_s_50.model.CRegion;
import com.example.demo_s_50.repository.CountryRepository;
import com.example.demo_s_50.repository.RegionRepository;

@Service
public class RegionService {
    @Autowired
    private RegionRepository regionRepository;

    @Autowired
    private CountryRepository countryRepository;

    public CRegion createRegion(Long id, CRegion cRegion) {
        Optional<CCountry> countryData = countryRepository.findById(id);
        CRegion newRole = new CRegion();
        newRole.setRegionName(cRegion.getRegionName());
        newRole.setRegionCode(cRegion.getRegionCode());
        newRole.setCountry(cRegion.getCountry());

        CCountry _country = countryData.get();
        newRole.setCountry(_country);
        newRole.setCountryName(_country.getCountryName());

        CRegion savedRole = regionRepository.save(newRole);
        return savedRole;
    }

    public CRegion updateRegion(Long id, CRegion cRegion) {
        Optional<CRegion> regionData = regionRepository.findById(id);
        CRegion newRegion = regionData.get();
        newRegion.setRegionName(cRegion.getRegionName());
        newRegion.setRegionCode(cRegion.getRegionCode());
        CRegion savedRegion = regionRepository.save(newRegion);
        return savedRegion;
    }

}
