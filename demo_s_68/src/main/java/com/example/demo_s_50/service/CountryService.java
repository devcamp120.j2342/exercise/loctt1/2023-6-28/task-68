package com.example.demo_s_50.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo_s_50.model.CCountry;
import com.example.demo_s_50.repository.CountryRepository;



@Service
public class CountryService {
    @Autowired
    CountryRepository countryRepository;

    public CCountry createCountry(CCountry cCountry) {
        CCountry c = new CCountry();
        c.setCountryName(cCountry.getCountryName());
        c.setCountryCode(cCountry.getCountryCode());
        c.setRegions(cCountry.getRegions());
        CCountry savedCountry = countryRepository.save(c);
        return savedCountry;
    }

    public CCountry updateCountry(Long id, CCountry cCountry) {
        Optional<CCountry> countryData = countryRepository.findById(id);
        CCountry newCountry = countryData.get();
        newCountry.setCountryName(cCountry.getCountryName());
        newCountry.setCountryCode(cCountry.getCountryCode());
        newCountry.setRegions(cCountry.getRegions());
        CCountry savedCountry = countryRepository.save(newCountry);
        return savedCountry;
    }
}
