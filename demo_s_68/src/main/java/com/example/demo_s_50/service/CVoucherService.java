package com.example.demo_s_50.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo_s_50.model.CVoucher;
import com.example.demo_s_50.repository.IVoucherRepository;

@Service
public class CVoucherService {
    @Autowired
    IVoucherRepository pVoucherRepository;

    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }

    public CVoucher postVoucherList(CVoucher pVouchers) {
        pVouchers.setNgayTao(new Date());
        pVouchers.setNgayCapNhat(null);
        CVoucher _vouchers = pVoucherRepository.save(pVouchers);
        return _vouchers;
    }

    public CVoucher putVoucherList(long id, CVoucher pVouchers) {
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
        CVoucher voucher = voucherData.get();
        voucher.setMaVoucher(pVouchers.getMaVoucher());
        voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
        voucher.setNgayCapNhat(new Date());
        return voucher;
    }

    public ResponseEntity<CVoucher> deleteVoucherList(long id) {
        pVoucherRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
