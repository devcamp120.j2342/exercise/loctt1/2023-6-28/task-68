package com.example.demo_s_50.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "region")
public class CRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, name = "region_code")
    private String regionCode;

    @Column(name = "region_name")
    private String regionName;
    @ManyToOne
    @JsonIgnore
    private CCountry country;

    @OneToMany(targetEntity = CRegion.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "region_id")
    private List<CDistrict> districts;

    @Transient
    private String countryName;

    @JsonIgnore
    public String getCountryName() {
        return getCountry().getCountryName();
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    @JsonIgnore
    public CCountry getCountry() {
        return country;
    }

    public void setCountry(CCountry cCountry) {
        this.country = cCountry;
    }

    public List<CDistrict> getDistricts() {
        return districts;
    }

    public void setDistricts(List<CDistrict> districts) {
        this.districts = districts;
    }
}
