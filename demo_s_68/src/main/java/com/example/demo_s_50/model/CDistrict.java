package com.example.demo_s_50.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "districts")
public class CDistrict {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(unique = true, name = "district_code")
    private String districtCode;
    @Column(name = "district_name")
    private String districtName;
    @ManyToOne
    @JsonIgnore
    private CRegion region;

    public CDistrict() {
    }

    public CDistrict(String districtCode, String districtName) {
        this.districtCode = districtCode;
        this.districtName = districtName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    @JsonIgnore
    public CRegion getRegion() {
        return region;
    }

    public void setRegion(CRegion region) {
        this.region = region;
    }

}
