package com.example.demo_s_50.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo_s_50.model.CRegion;

@Repository
public interface RegionRepository extends JpaRepository<CRegion, Long> {
	List<CRegion> findByCountryId(Long countryId);

	Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
}
