package com.example.demo_s_50.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_s_50.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {

}
