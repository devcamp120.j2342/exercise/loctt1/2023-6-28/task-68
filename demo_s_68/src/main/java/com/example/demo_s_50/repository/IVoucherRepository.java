package com.example.demo_s_50.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_s_50.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
