package com.example.demo_s_50.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_s_50.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {

    List<COrder> findByCUserId(Long userId);

    Optional<COrder> findByIdAndCUserId(Long orderId, Long userId);

}
