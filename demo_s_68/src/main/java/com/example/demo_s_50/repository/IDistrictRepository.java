package com.example.demo_s_50.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_s_50.model.CDistrict;


public interface IDistrictRepository extends JpaRepository<CDistrict, Long> {

    List<CDistrict> findByRegionId(Long regionId);

}
