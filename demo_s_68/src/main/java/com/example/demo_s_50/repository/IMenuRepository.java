package com.example.demo_s_50.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_s_50.model.CMenu;

public interface IMenuRepository extends JpaRepository<CMenu,Long>{
    
}
